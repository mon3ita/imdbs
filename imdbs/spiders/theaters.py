# -*- coding: utf-8 -*-
import scrapy

from ..items import ImdbsItem


class TheatersSpider(scrapy.Spider):
    name = "theaters"
    allowed_domains = ["www.imdb.com"]
    start_urls = ["https://www.imdb.com/movies-coming-soon/?ref_=inth_ov_cs_sm"]

    def parse(self, response):
        movies = response.css("table.nm-title-overview-widget-layout")

        if movies:
            for movie in movies:
                movie_item = ImdbsItem()

                title = (
                    movie.css("td.overview-top > h4 > a::text").extract_first().strip()
                )

                movie_item["title"] = title[: title.find("(")].strip()
                movie_item["year"] = title[title.find("(") + 1 : title.find(")")]

                try:
                    movie_item["time"] = (
                        movie.css("p.cert-runtime-genre > time::text")
                        .extract_first()
                        .strip()
                    )
                except:
                    movie_item["time"] = "0"

                genres = movie.css("p.cert-runtime-genre")
                movie_item["genre"] = ""
                for genre in genres:
                    movie_item["genre"] += genre.css("span::text").extract_first() or ""

                try:
                    movie_item["genre"] = movie_item["genre"].strip()
                except:
                    movie_item["genre"] = ""

                try:
                    movie_item["rating"] = (
                        movie.css(".metascore.mixed::text").extract_first().strip()
                    )
                except:
                    movie_item["rating"] = "0"

                movie_item["description"] = (
                    movie.css("div.outline::text").extract_first().strip()
                )

                included = movie.css(".txt-block")

                try:
                    movie_item["director"] = (
                        included[0].css("span > a::text").extract_first().strip()
                    )
                except:
                    movie_item["director"] = ""

                actors = []
                for actor in included[1:].css("a"):
                    actor_ = {}
                    actor_["name"] = actor.css("::text").extract_first()
                    actors.append(actor_)

                movie_item["actors"] = actors

                yield movie_item

            next_page = response.css(".see-more > a::attr(href)").extract()
            if len(next_page) > 1:
                next_page = "https://{}{}?ref_=cs_dt_nx".format(
                    self.allowed_domains[0], next_page[-1]
                )
                yield scrapy.Request(next_page, callback=self.parse)
