# -*- coding: utf-8 -*-
import scrapy

from ..items import ImdbsItem


class OscarSpider(scrapy.Spider):
    name = "oscar"
    allowed_domains = ["www.imdb.com"]
    start_urls = [
        "https://www.imdb.com/search/title/?groups=oscar_best_picture_winners&count=100"
    ]

    def parse(self, response):
        movies = response.css("div.lister-list > div.lister-item.mode-advanced")
        for movie in movies:
            item = ImdbsItem()
            item["title"] = movie.css(".lister-item-header > a::text").extract_first()
            year = movie.css(
                ".lister-item-year.text-muted.unbold::text"
            ).extract_first()
            item["year"] = year[1 : len(year) - 1]

            description = movie.css("p.text-muted::text").extract()
            item["description"] = " ".join(
                [part.strip() for part in description]
            ).strip()

            try:
                item["rating"] = (
                    movie.css(".metascore.favorable::text").extract_first().strip()
                )
            except:
                item["rating"] = "0"

            included = movie.css(".lister-item-content > p > a::text").extract()
            item["director"] = included[0]
            included.pop()
            item["actors"] = [{"name": actor.strip()} for actor in included]

            yield item
