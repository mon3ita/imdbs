# -*- coding: utf-8 -*-
import scrapy

from ..items import ImdbsItem


class PopularSpider(scrapy.Spider):
    name = "popular"
    allowed_domains = ["www.imdb.com"]
    start_urls = ["https://www.imdb.com/chart/moviemeter?ref_=nv_mv_mpm"]

    def parse(self, response):
        movie_table = response.css(".chart.full-width")
        for movie in movie_table.css("tr")[1:]:
            item = ImdbsItem()

            item["title"] = (
                movie.css("td.titleColumn > a ::text").extract_first().strip()
            )

            year = movie.css("span.secondaryInfo::text").extract_first().strip()
            item["year"] = year[1 : len(year) - 1]

            movie_link = movie.css("td.titleColumn > a::attr(href)").extract_first()
            full_link = "https://{}{}".format(self.allowed_domains[0], movie_link)
            yield scrapy.Request(
                full_link,
                callback=self.parse_movie_info,
                meta={"item": item, "url": full_link},
            )

    def parse_movie_info(self, response):

        item = response.meta["item"]
        item["description"] = (
            response.css("div.plot_summary > div.summary_text::text")
            .extract_first()
            .strip()
        )

        try:
            item["rating"] = (
                response.css(
                    "div.metacriticScore.score_mixed.titleReviewBarSubItem > span::text"
                )
                .extract_first()
                .strip()
            )
        except:
            item["rating"] = "0"

        included = response.css("div.credit_summary_item")
        item["director"] = included[0].css("a::text").extract_first().strip()

        all_actors_link = included[-1].css("a::attr(href)").extract()[-1]
        if "fullcredits" in all_actors_link:
            full_link = "{}{}".format(response.meta["url"], all_actors_link)
            yield scrapy.Request(
                full_link, callback=self.parse_actors, meta={"item": item}
            )
        else:
            actors = []
            for actor in included[2:].css("a"):
                actor_ = {"name": actor.css("::text").extract_first().strip()}
                actors.append(actor_)
            item["actors"] = actors
            yield item

    def parse_actors(self, response):
        item = response.meta["item"]

        table = response.css("table.cast_list")
        item["actors"] = [
            {"name": actor.strip()} for actor in table.css("td > a::text").extract()
        ]
        return item
