# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ImdbsItem(scrapy.Item):

    title = scrapy.Field()
    year = scrapy.Field()
    rating = scrapy.Field()
    genre = scrapy.Field()
    time = scrapy.Field()
    description = scrapy.Field()
    actors = scrapy.Field()
    awards = scrapy.Field()
    director = scrapy.Field()
