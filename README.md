IMDBS
-----

Gets information for all movies from IMDB.

## How to test it

1. Download the project, by typing `git clone https://github.com/monzita/imdbs` on your cli.

2. You have two options:
	1. To make your own `virtualenv` in the project folder and install its dependencies in that environment.
	2. To try run it, by installing all dependencies globally.

*If you already have `scrapy`(version 1.8.0), and `pymongo`(version 3.9.0) on your system, skip this step.*

3. To install the dependencies of the project, type `pip install -r requirements.txt`.

4. The project uses `mongodb` for a db, so make sure that you have running instance of `mongo` on `localhost:27017`. *You can change that in the `settings.py` file.*

That's it, now type `scrapy crawl [spider]`, where `spider` currently can be:

*	`theaters`, which will give you latest and future movies in the theaters.
*	`popular`, which gives you movies from the top list in IMDB.
*	`oscar`, which gives you all best picture-winning movies.

## LICENSE

MIT

